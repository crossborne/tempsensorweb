var express = require('express');
var bodyParser = require('body-parser');
var jade = require('jade');
var sequelize = require('sequelize');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

if (process.env.DEVELOPMENT) {
    var i = 0;
    setInterval(function() {
        var item = {
            "createdAt": new Date(),
            "temp": i++
        };
        Temperature.create(item).success(function() {});
    }, 5000);
}

app.set('view engine', 'jade');

app.use('/', express.static('./public'));
app.use('/', express.static('./bower_components'));

// setup and connect db
var match = 'mysql://tempsensoruser:tempsensorpass@euve64394.serverprofi24.eu/tempsensorweb?reconnect=true'.match(/mysql:\/\/([^:]+):([^@]*)@([^:]+):?(\d+)?\/(\w+)/);
connection = new sequelize(match[5], match[1], match[2], {
    dialect: 'mysql',
    port: match[4],
    host: match[3],
    logging: false
});

var Temperature = connection.define('Temperature', {
    createdAt: sequelize.DATE,
    temp: sequelize.FLOAT
}, {
    timestamps: false,
    tableName: 'temperature',
    indexes: [
        {
            name: 'dateIndex',
            fields: ['createdAt']
        }
    ]
});
Temperature.sync();

// routes

app.post('/update', function(req, res) {
    //console.log('templota omg', req.body.value);
    var item = {
        "createdAt": new Date(),
        "temp": req.body.value || -1
    };
    Temperature.create(item).success(function() {
        res.send('saved');
    });
});

app.get('/api/:lastId?', function(req, res) {
    if (!req.params.lastId) {
        var minusDay = new Date();
        minusDay = new Date(minusDay.getTime() - 3600 * 1000 * (process.env.HOURS_BACK || 14));
        var query = { where: ['createdAt > ?', minusDay] };
    } else {
        var query = { where: ['id > ?', req.params.lastId] };
    }

    Temperature.findAll(query).success(function(data) {
        res.json(data);
    });
});

app.get('/', function(req, res) {
    res.render('index', { lastId: '' });
});

app.get('/:year/:month?/:day?/:hours?', function(req, res) {
    var date = new Date(req.params.year + '-' + (req.params.month || 1) + '-' + (req.params.day || 1) + ' ' + (req.params.hours || '00') + ':00:00');
    Temperature.find({ where: ['createdAt > ?', date], orderBy: ['createdAt', 'ASC'] }).success(function(temp) {
        //res.redirect('/' + (temp ? temp.id : ''));
        res.render('index', { lastId: (temp ? temp.id : '') });
    });
});

// start server

app.listen(process.env.PORT || 8000, function() {
    console.log('listening...', process.env.PORT || 8000);
});
